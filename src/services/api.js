import axios from "axios";

export const api = axios.create({
  baseURL: "https://huia-api-default-rtdb.firebaseio.com/.json",
});
